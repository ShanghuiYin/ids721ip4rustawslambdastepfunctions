use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    // Process purchase-specific data
    let details = "Processed purchase details";

    // Prepare a purchase-specific message
    let message = format!("Purchase successful: {}", details);

    // Construct the HTTP response
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/plain")
        .body(message.into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
