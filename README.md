IDS721 Spring 2024 Individual Project 4 - Rust AWS Lambda and Step Functions
===============

## Demo Video

[Youtube Video Here](https://youtu.be/mlllADi07OI)
&nbsp;&nbsp;![YouTube Video Views](https://img.shields.io/youtube/views/mlllADi07OI)

> Screenshot

![img.png](img.png)

> The diagram represents a conditional workflow where the path of execution is determined by the type of transaction being processed—either a purchase or a refund. After the specific handler for the transaction type is invoked, both paths lead to a common result handler before the workflow ends.

## Requirements:

- Rust AWS Lambda function
- Step Functions workflow coordinating Lambdas
- Orchestrate data processing pipeline

## Rust Lambda Functionality

Three Lambda functions are implemented in Rust. 

- Purchase Handler: This Lambda function will perform some simple data processing specific to a purchase and return a corresponding result.
- Refund Handler: Similarly, this function will process refund-related data and return a different result.
- Result Handler: This function will take the output of either the Purchase Handler or the Refund Handler and generate a final output.



## Step Functions Workflow

1. Start: The initial state of the workflow.

2. Choice State (Choice): This is a decision node that routes the workflow based on the value of the input. The decision is made on the type attribute of the input.

3. There are two branches depending on the type attribute of the input:

   - If $.type is equal to "PURCHASE":
     - Lambda: Invoke Purchase Handler: This state represents the invocation of an AWS Lambda function that handles purchase actions. 
     - After the purchase action is handled, the workflow proceeds to another Lambda function:
     - Lambda: Invoke Result Handler: This Lambda function presumably handles the results or outcomes of the Purchase Handler function.
   - If $.type is equal to "REFUND":
     - Lambda: Invoke Refund Handler: In this branch, a different AWS Lambda function is invoked to handle refund actions.

4. Both branches converge after the handling functions complete their tasks, leading to the End state, which signifies the completion of the workflow.

![img_3.png](img_3.png)

## Data Processing Pipeline

Trigger the Step Functions workflow to start the left data processing pipeline.
```json
{
  "type": "PURCHASE"
}
```

The result of the Step Functions workflow is shown below.
```json
{
  "result": "PURCHASE_PROCESSED_SUCCESS"
}
```
![img_2.png](img_2.png)


Trigger the Step Functions workflow to start the right data processing pipeline.
```json
{
  "type": "REFUND"
}
```

The result of the Step Functions workflow is shown below.
```json
{
  "result": "REFUND_PROCESSED_SUCCESS"
}
```

![img_4.png](img_4.png)





## Reference

1. https://www.cargo-lambda.info/guide/getting-started.html
2. https://aws.amazon.com/cn/step-functions/
3. https://docs.aws.amazon.com/lambda/latest/dg/lambda-stepfunctions.html