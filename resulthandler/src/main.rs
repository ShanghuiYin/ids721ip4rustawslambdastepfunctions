use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
// Extract the type of handler that was executed (purchase or refund)
    let handler_type = event
        .headers_ref()
        .and_then(|headers| headers.get("X-Handler-Type"))
        .and_then(|value| value.to_str().ok())
        .unwrap_or("unknown");

    // Prepare a result message based on the handler type
    let result_details = if handler_type == "purchase" {
        "Finalized purchase result"
    } else if handler_type == "refund" {
        "Finalized refund result"
    } else {
        "Unknown handler result"
    };

    // Construct the HTTP response
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/plain")
        .body(result_details.into())
        .map_err(Box::new)?;
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();

    run(service_fn(function_handler)).await
}
